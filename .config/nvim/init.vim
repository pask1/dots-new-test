" Load config modules
source $HOME/.config/nvim/sets.vim
source $HOME/.config/nvim/plugs.vim
source $HOME/.config/nvim/maps.vim

" Load plugin-specific config modules
source $HOME/.config/nvim/plug-config/fzf.vim
source $HOME/.config/nvim/plug-config/vimtex.vim
source $HOME/.config/nvim/plug-config/coc.vim

" Themes (source from .config/nvim/themes instead of
" doing "colorscheme ..." since some themes have
" specific settings)
source $HOME/.config/nvim/themes/nord.vim
