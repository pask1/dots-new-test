set formatoptions-=cro " Stop newline continuation of comments
set t_ut=              " Fix colorschemes inside tmux

syntax enable
set nocompatible
set noerrorbells
"set noshowmode
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu rnu
set nowrap
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set splitbelow
set splitright

" Add system clipboard to default clipboard
" This requires xclip on X11 and wl-clipboard on Wayland
set clipboard+=unnamedplus

set sessionoptions=blank,buffers,curdir,folds,tabpages

" These are recommended by restore_view.vim
set viewoptions=cursor,folds,slash,unix
let g:skipview_files = ['*\.vim']

" Automatically save folds
"autocmd BufWinLeave ?* mkview!
"autocmd BufWinEnter ?* silent loadview
