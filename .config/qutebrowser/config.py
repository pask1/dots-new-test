# Documentation:
#   qute://help/configuring.html
#   qute://help/settings.html

import subprocess

# Settings in no particular order
config.load_autoconfig(True)
c.auto_save.interval = 15000
c.auto_save.session = True
c.session.lazy_restore = True
c.backend = 'webengine'
c.content.blocking.enabled = True
c.content.blocking.hosts.lists = ['https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts']
c.changelog_after_upgrade = 'minor'
c.url.auto_search = 'naive'
c.url.open_base_url = True
c.url.default_page = 'https://www.google.se/'
c.url.searchengines = {
    'DEFAULT': 'https://www.google.se/search?q={}',
    'ggl':     'https://www.google.se/search?q={}',
    'ddg':     'https://duckduckgo.com/?q={}'
}
c.completion.cmd_history_max_items = 5000
c.completion.open_categories = ['bookmarks', 'history']
c.completion.height = '50%'
c.completion.shrink = True
c.statusbar.widgets = ['keypress', 'url', 'progress']
c.tabs.show = 'multiple'
c.tabs.indicator.width = 0
c.tabs.title.format = '{audio}{index}: {current_title}'
c.tabs.title.format_pinned = '{index}'
c.tabs.padding = {'top': 5, 'bottom': 5, 'left': 5, 'right': 5}
c.downloads.position = 'bottom'
c.input.insert_mode.auto_load = True
#c.spellcheck.languages = ['en-US', 'sv-SE', 'ru-RU']
c.editor.command = ['st', '-e', 'nvim', '{file}', '-c', 'normal {line}G{column0}l']

# Read colors from xresourcess
def read_xresources(prefix):
    props = {}
    x = subprocess.run(['xrdb', '-query'], stdout=subprocess.PIPE)
    lines = x.stdout.decode().split('\n')
    for line in filter(lambda l : l.startswith(prefix), lines):
        prop, _, value = line.partition(':\t')
        props[prop] = value
    return props

xresources = read_xresources('*')

# Tabs colors
c.colors.tabs.even.bg = xresources['*.background']
c.colors.tabs.even.fg = xresources['*.foreground']
c.colors.tabs.odd.bg = xresources['*.background']
c.colors.tabs.odd.fg = xresources['*.foreground']
c.colors.tabs.pinned.even.bg = xresources['*.background']
c.colors.tabs.pinned.even.fg = xresources['*.foreground']
c.colors.tabs.pinned.odd.bg = xresources['*.background']
c.colors.tabs.pinned.odd.fg = xresources['*.foreground']
c.colors.tabs.pinned.selected.even.bg = xresources['*.background-even']
c.colors.tabs.pinned.selected.even.fg = 'white'
c.colors.tabs.pinned.selected.odd.bg = xresources['*.background-even']
c.colors.tabs.pinned.selected.odd.fg = 'white'
c.colors.tabs.selected.even.bg = xresources['*.background-even']
c.colors.tabs.selected.even.fg = 'white'
c.colors.tabs.selected.odd.bg = xresources['*.background-even']
c.colors.tabs.selected.odd.fg = 'white'

# Completion menu colors
c.colors.completion.fg = [xresources['*.foreground'], xresources['*.foreground'], xresources['*.foreground']]
c.colors.completion.category.bg = xresources['*.background']
c.colors.completion.category.fg = 'white'
c.colors.completion.category.border.top = xresources['*.background']
c.colors.completion.category.border.bottom = xresources['*.background']
c.colors.completion.even.bg = xresources['*.background-even']
c.colors.completion.odd.bg = xresources['*.background']
c.colors.completion.item.selected.bg = xresources['*.foreground']
c.colors.completion.item.selected.fg = xresources['*.background']
c.colors.completion.item.selected.border.bottom = xresources['*.foreground']
c.colors.completion.item.selected.border.top = xresources['*.foreground']
c.colors.completion.item.selected.match.fg = '#ff4444'
c.colors.completion.match.fg = xresources['*.color9']
c.colors.completion.scrollbar.bg = xresources['*.background']
c.colors.completion.scrollbar.fg = xresources['*.background-even']

# Status bar colors
c.colors.statusbar.normal.bg = xresources['*.background']
c.colors.statusbar.normal.fg = xresources['*.foreground']
c.colors.statusbar.insert.bg = xresources['*.background']
c.colors.statusbar.insert.fg = xresources['*.color10']
c.colors.statusbar.passthrough.fg = xresources['*.color12']
c.colors.statusbar.passthrough.bg = xresources['*.background']
c.colors.statusbar.command.fg = xresources['*.foreground']
c.colors.statusbar.command.bg = xresources['*.background']
c.colors.statusbar.caret.bg = xresources['*.color5']
c.colors.statusbar.caret.fg = 'white'
c.colors.statusbar.caret.selection.bg = xresources['*.color5']
c.colors.statusbar.caret.selection.fg = 'white'
c.colors.statusbar.url.fg = xresources['*.color8']
c.colors.statusbar.url.hover.fg = 'white'
c.colors.statusbar.url.success.http.fg = xresources['*.color8']
c.colors.statusbar.url.success.https.fg = xresources['*.foreground']
c.colors.statusbar.url.error.fg = 'orange'
c.colors.statusbar.private.bg = '#8000d7'
c.colors.statusbar.private.fg = 'black'

# Context menu colors
c.colors.contextmenu.menu.bg = xresources['*.background']
c.colors.contextmenu.menu.fg = xresources['*.foreground']
c.colors.contextmenu.selected.bg = xresources['*.color4']
c.colors.contextmenu.selected.fg = xresources['*.color0']
c.colors.contextmenu.disabled.bg = xresources['*.color10']
c.colors.contextmenu.disabled.fg = xresources['*.color12']

# Message colors
c.colors.messages.error.bg = xresources['*.color1']
c.colors.messages.error.border = xresources['*.color1']
c.colors.messages.error.fg = 'white'
c.colors.messages.info.bg = xresources['*.background']
c.colors.messages.info.border = xresources['*.background']
c.colors.messages.info.fg = xresources['*.foreground']
c.colors.messages.warning.bg = 'darkorange'
c.colors.messages.warning.border = 'darkorange'
c.colors.messages.warning.fg = 'black'

# Misc colors
c.colors.downloads.bar.bg = xresources['*.background']
c.colors.downloads.start.bg = xresources['*.color2']
c.colors.downloads.stop.bg = xresources['*.color4']
c.colors.prompts.bg = xresources['*.color8']
c.colors.prompts.fg = xresources['*.foreground']
c.colors.keyhint.bg = 'rgba(0, 43, 54, 80%)'
c.colors.keyhint.fg = '#FFFFFF'
c.colors.keyhint.suffix.fg = '#FFFF00'

## Which algorithm to use for modifying how colors are rendered with
## darkmode. The `lightness-cielab` value was added with QtWebEngine 5.14
## and is treated like `lightness-hsl` with older QtWebEngine versions.
## Type: String
## Valid values:
##   - lightness-cielab: Modify colors by converting them to CIELAB color space and inverting the L value. Not available with Qt < 5.14.
##   - lightness-hsl: Modify colors by converting them to the HSL color space and inverting the lightness (i.e. the "L" in HSL).
##   - brightness-rgb: Modify colors by subtracting each of r, g, and b from their maximum value.
c.colors.webpage.darkmode.algorithm = 'lightness-cielab'

## Contrast for dark mode. This only has an effect when
## `colors.webpage.darkmode.algorithm` is set to `lightness-hsl` or
## `brightness-rgb`.
## Type: Float
# c.colors.webpage.darkmode.contrast = 0.0

## Render all web contents using a dark theme. Example configurations
## from Chromium's `chrome://flags`:  - "With simple HSL/CIELAB/RGB-based
## inversion": Set   `colors.webpage.darkmode.algorithm` accordingly.  -
## "With selective image inversion": Set
## `colors.webpage.darkmode.policy.images` to `smart`.  - "With selective
## inversion of non-image elements": Set
## `colors.webpage.darkmode.threshold.text` to 150 and
## `colors.webpage.darkmode.threshold.background` to 205.  - "With
## selective inversion of everything": Combines the two variants   above.
## Type: Bool
c.colors.webpage.darkmode.enabled = False

## Which pages to apply dark mode to. The underlying Chromium setting has
## been removed in QtWebEngine 5.15.3, thus this setting is ignored
## there. Instead, every element is now classified individually.
## Type: String
## Valid values:
##   - always: Apply dark mode filter to all frames, regardless of content.
##   - smart: Apply dark mode filter to frames based on background color.
c.colors.webpage.darkmode.policy.page = 'smart'

## Value to send in the `Accept-Language` header. Note that the value
## read from JavaScript is always the global value.
## Type: String
# c.content.headers.accept_language = 'en-US,en;q=0.9'

## List of user stylesheet filenames to use.
## Type: List of File, or File
# c.content.user_stylesheets = []

## Command (and arguments) to use for selecting a single folder in forms.
## The command should write the selected folder path to the specified
## file or stdout. The following placeholders are defined: * `{}`:
## Filename of the file to be written to. If not contained in any
## argument, the   standard output of the command is read instead.
## Type: ShellCommand
# c.fileselect.folder.command = ['xterm', '-e', 'ranger', '--choosedir={}']

## Handler for selecting file(s) in forms. If `external`, then the
## commands specified by `fileselect.single_file.command` and
## `fileselect.multiple_files.command` are used to select one or multiple
## files respectively.
## Type: String
## Valid values:
##   - default: Use the default file selector.
##   - external: Use an external command.
# c.fileselect.handler = 'default'

## Command (and arguments) to use for selecting multiple files in forms.
## The command should write the selected file paths to the specified file
## or to stdout, separated by newlines. The following placeholders are
## defined: * `{}`: Filename of the file to be written to. If not
## contained in any argument, the   standard output of the command is
## read instead.
## Type: ShellCommand
# c.fileselect.multiple_files.command = ['xterm', '-e', 'ranger', '--choosefiles={}']

## Command (and arguments) to use for selecting a single file in forms.
## The command should write the selected file path to the specified file
## or stdout. The following placeholders are defined: * `{}`: Filename of
## the file to be written to. If not contained in any argument, the
## standard output of the command is read instead.
## Type: ShellCommand
# c.fileselect.single_file.command = ['xterm', '-e', 'ranger', '--choosefile={}']

## Font used for the context menu. If set to null, the Qt default is
## used.
## Type: Font
c.fonts.contextmenu = None
c.fonts.default_family = ['Fira Code']
c.fonts.default_size = '10pt'

## Font used for prompts.
## Type: Font
# c.fonts.prompts = 'default_size sans-serif'

## Font family for cursive fonts.
## Type: FontFamily
# c.fonts.web.family.cursive = ''

## Font family for fantasy fonts.
## Type: FontFamily
# c.fonts.web.family.fantasy = ''

## Font family for fixed fonts.
## Type: FontFamily
# c.fonts.web.family.fixed = ''

## Font family for sans-serif fonts.
## Type: FontFamily
# c.fonts.web.family.sans_serif = ''

## Font family for serif fonts.
## Type: FontFamily
# c.fonts.web.family.serif = ''

## Font family for standard fonts.
## Type: FontFamily
# c.fonts.web.family.standard = ''

## Default font size (in pixels) for regular text.
## Type: Int
# c.fonts.web.size.default = 16

## Default font size (in pixels) for fixed-pitch text.
## Type: Int
# c.fonts.web.size.default_fixed = 13

## Force software rendering for QtWebEngine. This is needed for
## QtWebEngine to work with Nouveau drivers and can be useful in other
## scenarios related to graphic issues.
## Type: String
## Valid values:
##   - software-opengl: Tell LibGL to use a software implementation of GL (`LIBGL_ALWAYS_SOFTWARE` / `QT_XCB_FORCE_SOFTWARE_OPENGL`)
##   - qt-quick: Tell Qt Quick to use a software renderer instead of OpenGL. (`QT_QUICK_BACKEND=software`)
##   - chromium: Tell Chromium to disable GPU support and use Skia software rendering instead. (`--disable-gpu`)
##   - none: Don't force software rendering.
# c.qt.force_software_rendering = 'none'

## Turn on Qt HighDPI scaling. This is equivalent to setting
## QT_AUTO_SCREEN_SCALE_FACTOR=1 or QT_ENABLE_HIGHDPI_SCALING=1 (Qt >=
## 5.14) in the environment. It's off by default as it can cause issues
## with some bitmap fonts. As an alternative to this, it's possible to
## set font sizes and the `zoom.default` setting.
## Type: Bool
# c.qt.highdpi = False
